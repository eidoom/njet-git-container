FROM docker.io/library/debian:buster-slim

RUN apt update && \
        apt install -y git g++ gfortran make python vim autoconf automake libtool

WORKDIR /usr/local/src/

RUN git clone https://bitbucket.org/njet/njet.git

WORKDIR /usr/local/src/njet

RUN git config --local submodule.lib-qd.url 'https://bitbucket.org/njet/qd-library.git' && \
        git config --local submodule.lib-qcdloop1.url 'https://bitbucket.org/njet/qcdloop1.git' && \
        git submodule init && \
        git submodule update && \
        autoreconf -i && \
        mkdir build

WORKDIR /usr/local/src/njet/build

RUN FFLAGS='-std=legacy' CXXFLAGS='-std=c++11 -O2' ../configure --prefix=/usr/local/njet --enable-quaddouble && \
        make -j && \
        make -j check && \
        make install

WORKDIR /usr/local/src/njet
