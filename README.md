# [njet-git-container](https://gitlab.com/eidoom/njet-git-container)

Image with the latest [NJet](https://bitbucket.org/njet/njet) Git version.
Source and build files are included.
For testing.

## Get

Container image on [Docker Hub](https://hub.docker.com/r/eidoom/njet-git).

Get it with
```shell
docker pull eidoom/njet-git
```

Or on Fedora
```shell
podman pull docker.io/eidoom/njet-git
```
Similarly replace `docker` with `podman` for the following commands.

## Use

Interactive:
```shell
docker run -it --rm eidoom/njet-git bash
```

Alternative usage discussed on the [Rivet website](https://rivet.hepforge.org/trac/wiki/Docker) could be similarly used for NJet.

## Build

Build with
```shell
docker build -t eidoom/njet-git .
```
then push to Docker Hub with
```shell
docker push eidoom/njet-git
```

## See also

* [njet-container](https://gitlab.com/eidoom/njet-container)
* [njet-test-container](https://gitlab.com/eidoom/njet-test-container)
